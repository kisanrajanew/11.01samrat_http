#include "http_data.h"



#define LONG_PRINT_DELAY 600
#define PRINT_DELAY      500

extern volatile int current_r; // final value

extern volatile int voltage_ry; // final value


/**********************************************************************************
 * This function will send the voltage and current information to a server on cloud  
 **********************************************************************************/

void send_device_parameters_custom(char *url)
{
	int i=0;
	char url_buff[256];
        unsigned short current;
	unsigned short voltage;

        current= current_r;
	voltage= conv_ph2ph_volt(ADC_CH_VOLTAGE_RY);

	
	for (i=0;i<256;i++)
		{
			url_buff[i] = 0;
		}

	send_to_gsm_string("AT+GSN");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_UNKNOWN);


		sprintf(url_buff,"AT+HTTPPARA=\"URL\",\"%s/insert.php\?pump=%s&current=%d&voltage=%d\"",
	        url, response, current_r, voltage); 
		setup_http();
		send_http_data(url_buff);

	#if 0
		Delay_sec(GSM_DELAY);
		send_sms((unsigned char *)V_PH,(unsigned char *)url_buff);
		Delay_sec(GSM_DELAY);
	#endif

		_delay_ms(60000); 
	 //}
//return;
}




void setup_http()
	{

	/* AT+CREG=1 */        
	send_to_gsm_string("AT+CREG=1");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	print_ssd_delay("CREG",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);

	/* AT+SAPBR=3,1,"Contype","GPRS" */
	send_to_gsm_string("AT+SAPBR=3,1,\"Contype\",\"GPRS\"");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	print_ssd_delay("SAPBR-GPRS",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);

	if(0 != strcmp((char*)response, "OK"))
	{
	print_ssd_delay("SAPBR-GPR SERRor",0,PRINT_DELAY);
		return;	
	}
  	/* AT+SAPBR=3,1,"APN","internet" */                            
	send_to_gsm_string("AT+SAPBR=3,1,\"APN\",\"airtelgprs.com\"");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	print_ssd_delay("SAPBR-APN",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);

	if(0 != strcmp((char*)response, "OK"))
	{
	print_ssd_delay("SAPBR-APN-ERRor",0,PRINT_DELAY);
		return;	
	}
   
   	/* AT+SAPBR=1,1*/            
	send_to_gsm_string("AT+SAPBR=1,1");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	//_delay_ms(2000);
	print_ssd_delay("SAPBR-1-1",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);

if(0 != strcmp((char*)response, "OK"))
	{
	print_ssd_delay("SAPBR-1-1-ERRor",0,PRINT_DELAY);
		return;	
	}
   /* AT+SAPBR=2,1*/ 
	   send_to_gsm_string("AT+SAPBR=2,1");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	//_delay_ms(2000);
	print_ssd_delay("SAPBR-2-1",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);
if(0 != strcmp((char*)response, "OK"))
	{
	print_ssd_delay("SAPBR-2-1-ERRor",0,PRINT_DELAY);
		return;	
	}
   	_delay_ms(1000);
	
	return;
}

void send_http_data(char *url_buff)
{
 	/* 	Initializes HTTP service:
		AT+HTTPINIT */             
	send_to_gsm_string("AT+HTTPINIT");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	//_delay_ms(1000);

	print_ssd_delay("HTTPINIT",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);

                
	send_to_gsm_string("AT+HTTPPARA=\"CID\",1");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	print_ssd_delay("HTTPPARA-CID",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);

    	send_to_gsm_string(url_buff); 

	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	print_ssd_delay("HTTPPARA-URL",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);

            
	send_to_gsm_string("AT+HTTPACTION=0");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	print_ssd_delay("HTTPACTION-0",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);

	/* AT+HTTPREAD */
	send_to_gsm_string("AT+HTTPREAD");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
    //_delay_ms(30000);
	print_ssd_delay("HTTPREAD",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);
	

	/* AT+HTTPTERM */
	send_to_gsm_string("AT+HTTPTERM");
	recv_response(GSM_RESPONSE_TIMEOUT, GSM_RES_OK);
	//_delay_ms(5000);
	print_ssd_delay("HTTPTERM",0,PRINT_DELAY);
	print_ssd_delay((char *)response,0,LONG_PRINT_DELAY);

	return;
}
